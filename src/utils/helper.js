import Grid from '../components/Grid';
import List from '../components/List';
import {KEY_GRID, KEY_LIST} from '../constant/constant';

const keyToComponent = (key) => {
  switch (key) {
    case KEY_GRID:
      return Grid;
    case KEY_LIST:
      return List;
    default:
      return null;
  }
};

export {keyToComponent};
