import React from 'react';
import {View, Text} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import styles from './grid-styles';
import GridItem from './GridItem';

const Grid = ({data, header, columns, disabled, overlayStyle, navigation}) => {
  return (
    <View style={styles.container}>
      {header && (
        <View style={styles.header}>
          <Text style={styles.txtHeader}>{header}</Text>
        </View>
      )}
      <FlatList
        data={data}
        renderItem={({item}) => (
          <GridItem
            {...item}
            columns={columns}
            overlayStyle={overlayStyle}
            disabled={disabled}
            onPress={() => navigation.push('Detail', {item, sectionData: data})}
          />
        )}
        keyExtractor={(_, i) => `${i}`}
        numColumns={columns}
      />
    </View>
  );
};

export default Grid;
