import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    marginVertical: 12,
  },
  txtTitle: {
    fontSize: 18,
    fontWeight: '600',
    marginVertical: 12,
  },
});

export default styles;
