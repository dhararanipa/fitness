import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    marginTop: 12,
  },
  header: {
    marginLeft: 8,
  },
  txtHeader: {
    fontSize: 18,
    fontWeight: '700',
    paddingVertical: 4,
  },
});

export default styles;
