import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import styles from './listItem-styles';

const ListItem = ({title, image, onPress}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.imgContainer}>
        <Image source={{uri: image}} style={styles.img} />
      </View>
      <View style={styles.textContainer}>
        <Text>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};

ListItem.defaultProps = {
  onPress: () => {},
};

export default ListItem;
