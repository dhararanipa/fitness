import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    padding: 8,
  },
  imgContainer: {
    height: 120,
    width: 120,
    borderRadius: 8,
    overflow: 'hidden',
  },
  img: {
    width: '100%',
    height: '100%',
  },
  textContainer: {
    flex: 1,
    paddingLeft: 8,
    paddingTop: 4,
  },
});

export default styles;
