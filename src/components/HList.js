import React from 'react';
import {View, Text, FlatList} from 'react-native';
import GridItem from './GridItem';
import styles from './hList-styles';

const HList = ({data, navigation}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.txtTitle}>You May Also Like</Text>
      <FlatList
        data={data}
        renderItem={({item}) => (
          <GridItem
            {...item}
            columns={2}
            onPress={() => navigation.push('Detail', {item, sectionData: data})}
          />
        )}
        keyExtractor={(_, i) => `${i}`}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
};

export default HList;
