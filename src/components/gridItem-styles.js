import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: 200,
    margin: 4,
    borderRadius: 8,
    overflow: 'hidden',
  },
  imgContainer: {
    position: 'absolute',
    height: '100%',
    width: '100%',
  },
  img: {
    height: '100%',
    width: '100%',
    resizeMode: 'cover',
  },
  txtTitle: {
    color: '#fff',
    fontSize: 16,
  },
  overlay: {
    backgroundColor: '#333333aa',
    width: '100%',
    height: '100%',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    padding: 8,
  },
});

export default styles;
