import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'red',
  },
  header: {
    marginLeft: 8,
  },
  txtHeader: {
    fontSize: 18,
    fontWeight: '700',
    paddingVertical: 4,
  },
});

export default styles;
