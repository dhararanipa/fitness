import React from 'react';
import {View, Text, FlatList} from 'react-native';
import styles from './list-styles';
import ListItem from './ListItem';

const List = ({data, header, navigation}) => {
  return (
    <View>
      {header && (
        <View style={styles.header}>
          <Text style={styles.txtHeader}>{header}</Text>
        </View>
      )}
      <FlatList
        data={data}
        renderItem={({item}) => (
          <ListItem
            {...item}
            onPress={() => navigation.push('Detail', {item, sectionData: data})}
          />
        )}
        keyExtractor={(_, i) => `${i}`}
      />
    </View>
  );
};

export default List;
