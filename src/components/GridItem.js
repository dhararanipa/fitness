import React from 'react';
import {View, Text, Dimensions, Image, TouchableOpacity} from 'react-native';

import styles from './gridItem-styles';

const GridItem = ({title, image, columns, overlayStyle, disabled, onPress}) => {
  return (
    <TouchableOpacity
      style={[
        styles.container,
        {
          width: Dimensions.get('window').width / columns - 8,
          height: 150 / (columns * 0.5),
        },
      ]}
      activeOpacity={0.5}
      disabled={disabled}
      onPress={onPress}>
      <View style={styles.imgContainer}>
        <Image source={{uri: image}} style={styles.img} />
      </View>
      <View style={[styles.overlay, overlayStyle]}>
        <Text style={styles.txtTitle} numberOfLines={2}>
          {title}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

GridItem.defaultProps = {
  overlayStyle: {},
  disabled: false,
  onPress: () => {},
};

export default GridItem;
