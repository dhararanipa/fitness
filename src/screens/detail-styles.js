import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
  },
  txtTitle: {
    fontSize: 20,
    color: '#333333',
    fontWeight: '700',
    paddingVertical: 8,
    textAlign: 'justify',
  },
  txtDescription: {
    textAlign: 'justify',
    fontSize: 16,
  },
  videoContainer: {
    marginHorizontal: -16,
  },
  placeHolder: {
    height: 260,
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    width: '100%',
    height: '100%',
  },
  footer: {
    marginVertical: 16,
  },
  txtFooter: {
    fontSize: 18,
    fontWeight: '600',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  subTitle: {
    fontSize: 16,
  },
});

export default styles;
