import React from 'react';
import {View, VirtualizedList} from 'react-native';
import data from '../constant/sample';
import {keyToComponent} from '../utils/helper';

const Home = ({navigation}) => {
  return (
    <View>
      <VirtualizedList
        data={data}
        renderItem={({item}) => {
          const Component = keyToComponent(item.type);
          return <Component {...item} navigation={navigation} />;
        }}
        keyExtractor={(_, i) => `${i}`}
        getItemCount={(d) => d.length}
        getItem={(d, i) => d[i]}
      />
    </View>
  );
};

export default Home;
