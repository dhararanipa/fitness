import React, {useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  FlatList,
  Image,
  ActivityIndicator,
} from 'react-native';
import YoutubePlayer from 'react-native-youtube-iframe';
import GridItem from '../components/GridItem';
import styles from './detail-styles';

const Detail = ({route, navigation}) => {
  const {item} = route.params;
  const [isLoading, setLoading] = useState(true);

  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView
        contentContainerStyle={{flexGrow: 1}}
        style={styles.container}>
        <View style={styles.videoContainer}>
          {isLoading && (
            <View style={styles.placeHolder}>
              <ActivityIndicator />
            </View>
          )}
          <YoutubePlayer
            height={isLoading ? 0 : 260}
            videoId={item.video}
            onReady={() => setLoading(false)}
          />
        </View>
        <View style={{flex: 1}}>
          <Text style={styles.txtTitle}>{item.title}</Text>
          <Text style={styles.txtDescription}>{item.description}</Text>
        </View>

        <FlatList
          data={item.images}
          renderItem={({item}) => {
            return (
              <View
                style={{
                  height: 150,
                  width: 150,
                  margin: 10,
                  borderWidth: 1,
                  borderRadius: 15,
                  overflow: 'hidden',
                  borderColor: 'lightgrey',
                }}>
                <Image
                  source={{uri: item.image}}
                  style={{height: 150, width: 150}}
                />
              </View>
            );
          }}
          keyExtractor={(_, i) => `${i}`}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
        />
        <View style={{height: 40}} />
      </ScrollView>
    </SafeAreaView>
  );
};

export default Detail;
