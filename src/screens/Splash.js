import React, {Component} from 'react';
import {StyleSheet, Image, Text, View} from 'react-native';

class Splash extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.replace('Home');
    }, 1000);
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.txt}>GYM</Text>
        <Text style={styles.txt}>Equipments</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txt: {
    fontSize: 25,
    color: '#ac2424',
  },
});

export default Splash;
