import {KEY_GRID, KEY_LIST} from './constant';

const datas = [
  {
    title: 'Treadmill',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBTtLg8Ii1y_osUEVOXbI8vZ24jiVM1iqprw&usqp=CAU&ec=45761791',
    images: [
      {
        image:
          'https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1574800111-technogym-skillrun-unity-7000-grid-1574800107.jpg?crop=1xw:1xh;center,top&resize=320%3A%2A',
      },
      {
        image:
          'https://images-na.ssl-images-amazon.com/images/I/61SwQlI0AqL._SL1277_.jpg',
      },
      {
        image:
          'https://cdn.improb.com/wp-content/uploads/2019/12/RESOLVE-FITNESS-Reactive-Runner-Treadmill.jpg',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQP-Mq6pUHGcvLTvcxmrGYkfZjVuJ9fLRutxw&usqp=CAU',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRHrG25ZBymGCcyyOCTVCMAbkKw40FWeZGGjg&usqp=CAU',
      },
    ],
    description: `
    A treadmill is a device generally used for walking, running, or climbing while staying in the same place. Treadmills were introduced before the development of powered machines to harness the power of animals or humans to do work, often a type of mill operated by a person or animal treading the steps of a treadwheel to grind grain. In later times, treadmills were used as punishment devices for people sentenced to hard labor in prisons. The terms treadmill and treadwheel were used interchangeably for the power and punishment mechanisms.
    `,
    video: '8i3Vrd95o2k',
  },
  {
    title: 'Elliptical',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQspuaF2mV2oZw-WkEPEpdVedz_oVdCHIgHJg&usqp=CAU',
    images: [
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRC1Kdn6r2gI79Ue96032aviBrhLNJKpil8UQ&usqp=CAU',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvUirRqg06GFZHh-t0SuANUeBqE4m9bO0IeQ&usqp=CAU',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQaWEeeobv58KUAG2ehEnPg1hoIGqZT8d9M-g&usqp=CAU',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_M8kkHxTqhlSHgosyFI-Iie6b_nmHtuyiFg&usqp=CAU',
      },
      {
        image:
          'https://image.made-in-china.com/2f0j00GqhRZckBbLbH/Cybex-3-in-1-Obitrack-Magnetic-Elliptical-Machine-Cybex-Arc-Trainer.jpg',
      },
    ],
    description: `
    An elliptical trainer or cross-trainer (also called an X-trainer) is a stationary exercise machine used to stair climb, walk, or run without causing excessive pressure to the joints, hence decreasing the risk of impact injuries. For this reason, people with some injuries can use an elliptical to stay fit, as the low impact affects them little. Elliptical trainers offer a non-impact cardiovascular workout that can vary from light to high intensity based on the speed of the exercise and the resistance preference set by the user.
    `,
    video: 'ij87GXkcdho',
  },
  {
    title: 'Stationary Bicycle',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQjrmSZw666366gjhJG3m9KcOrHo4qyEBkXZg&usqp=CAU',
    images: [
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQhdBafVuxGFTCCnlqIv0xWMUC-vVJezdSCjw&usqp=CAU',
      },
      {
        image:
          'https://pyxis.nymag.com/v1/imgs/495/eff/54954885a98d65b1b6b4621ea340744690-sunny.rsquare.w600.jpg',
      },
      {
        image:
          'https://pyxis.nymag.com/v1/imgs/26a/142/3ee6e18f5350dba0c57102b60227fddbb4-schwinn-recumbent-exercise-bike.2x.rsquare.w600.jpg',
      },
      {
        image:
          'https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1598982500-cyclace-1598982465.jpg?crop=1xw:1xh;center,top&resize=768%3A%2A',
      },
      {
        image:
          'https://www.cascadehealthandfitness.com/wp-content/uploads/sites/4/2017/12/chf-pro-power-sideright.jpg',
      },
    ],
    description: `
    A stationary bicycle (also known as exercise bicycle, exercise bike, spinning bike, or exercycle) is a device used as exercise equipment for indoor cycling. It includes a saddle, pedals, and some form of handlebars arranged as on a stationary bicycle.

    A stationary bicycle is usually a special-purpose exercise machine resembling a bicycle without wheels.[3] It is also possible to adapt an ordinary bicycle for stationary exercise by placing it on bicycle rollers or a trainer. Rollers and trainers are often used by racing cyclists to warm up before racing, or to train on their own machines indoors.
    `,
    video: 'Ovlm9wWTk7Y',
  },
  {
    title: 'Aerobic Steppers',
    image:
      'https://www.flamanfitness.com/media/catalog/product/cache/1/small_image/cbcbef48e5e3bcce7c7ed908f20bc5b4/m/d/md_buddy_aerobic_step-1.jpg',
    images: [
      {
        image:
          'https://images-na.ssl-images-amazon.com/images/I/71sHg5TTVDL._SX425_.jpg',
      },
      {
        image:
          'https://i.pinimg.com/564x/41/b2/11/41b211bbd34fa896314ecf036c9b4468.jpg',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSb2E_LUcCxD85vdmVTvpsRgD9H6xLIrCWoNQ&usqp=CAU',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS41ETzuxtx-CCCR0Qt23TSGcl6EcdHl_jZ7g&usqp=CAU',
      },
      {
        image:
          'https://www.solefitness.sg/assets/images/aerobic%20stepper%20%20bench-1.jpg',
      },
    ],
    description: `
    Aerobics is a form of physical exercise that combines rhythmic aerobic exercise with stretching and strength training routines with the goal of improving all elements of fitness (flexibility, muscular strength, and cardio-vascular fitness). It is usually performed to music and may be practiced in a group setting led by an instructor (fitness professional), although it can be done solo and without musical accompaniment. With the goal of preventing illness and promoting physical fitness, practitioners perform various routines comprising a number of different dance-like exercises. Formal aerobics classes are divided into different levels of intensity and complexity and will have five components: warm-up (5–10 minutes), cardiovascular conditioning (25–30 minutes), muscular strength and conditioning (10–15 minutes), cool-down (5–8 minutes) and stretching and flexibility (5–8 minutes). Aerobics classes may allow participants to select their level of participation according to their fitness level. Many gyms offer a variety of aerobic classes. Each class is designed for a certain level of experience and taught by a certified instructor with a specialty area related to their particular class.
    `,
    video: 'TadkiFK9q0A',
  },
  {
    title: 'Cable Pulley',
    image:
      'https://5.imimg.com/data5/NG/BT/MY-12554023/cable-cross-over-pulley-gym-machine-500x500.jpg',
    images: [
      {
        image:
          'https://i.pinimg.com/originals/47/71/9d/47719d6df5edecf51396674028e450a9.png',
      },
      {
        image:
          'https://www.gymventures.com/content/images/wordpress/2016/12/Body-Solid-Pro-Clubline-Cable-Crossover.png',
      },
      {
        image:
          'https://5.imimg.com/data5/SU/GG/MY-35561483/single-pulley-500x500.jpg',
      },
      {
        image:
          'https://www.gymventures.com/content/images/wordpress/2016/12/Body-Solid-Pro-Dual-Cable-Column-Machine.png',
      },
      {
        image: 'https://m.media-amazon.com/images/I/41oIcG88z+L.jpg',
      },
    ],
    description: `A cable machine is an item of equipment used in weight training or functional training. It consists of a rectangular, vertically oriented steel frame about 3 metres wide and 2 metres high, with a weight stack attached via a cable and pulley system to one or more handles. The cables that connect the handles to the weight stacks run through adjustable pulleys that can be fixed at any height. This allows a variety of exercises to be performed on the apparatus. One end of the cable is attached to a perforated steel bar that runs down the centre of the weight stack. To select the desired amount of resistance, move the metal pin into the labelled hole in the weight stack. The other end of the cable forms a loop, which allows the user to attach the appropriate handle for the exercise. Most cable machines have a minimum of 20 pounds (~9 kilograms) of resistance in order to counterbalance the weight of the typical attachment.
    `,
    video: 'C3qPBk9C_uc',
  },
  {
    title: 'Indoor Rower',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTegw1Zjwhy47dhufdW-mE-SsfqRTDCCplF-Q&usqp=CAU',
    images: [
      {
        image:
          'https://www.verywellfit.com/thmb/eojEnucBb86tHSiPJuHsgYkjJ1o=/1500x1125/smart/filters:no_upscale()/_hero_SQ_Concept2-Model-E-Indoor-Rowing-Machine-1-6e05ba899b67418cbea437666aacbae2.jpg',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSV5U3ZqtSxMTm-d1PZgNmM8USEHkY5eT4hgg&usqp=CAU',
      },
      {
        image:
          'https://www.verywellfit.com/thmb/keer34sBD_tkiWv0DF4JVH5g-tU=/640x360/smart/filters:no_upscale()/Hydrorowernew-cd9dd0d51538428382b9462f24dc5abc.jpg',
      },
      {
        image:
          'https://images-na.ssl-images-amazon.com/images/I/61Y9wc4-uWL._AC_SX569_.jpg',
      },
      {
        image:
          'https://www.jtxfitness.com/pub/media/catalog/product/cache/aea7ede9c4f8eec24074fda02ef96ae3/j/t/jtx_ignite_air_indoor_rower_-_3_year_warranty.jpg',
      },
    ],
    description: ` An indoor rower, or rowing machine, is a machine used to simulate the action of watercraft rowing for the purpose of exercise or training for rowing. Indoor rowing has become established as a sport in its own right. The term "indoor rower" also refers to a participant in this sport.

    Modern indoor rowers are often known as ergometers, which is technically incorrect, as an ergometer is a device which measures the amount of work performed. The indoor rower is calibrated to measure the amount of energy the rower is using through their use of the equipment. Typically the display of the ergometer will show the time it takes to row 500m at each strokes power, also called split rate, or split.
    `,
    video: 'RH3wVAGbIhM',
  },
  {
    title: 'BARBELLS',
    image:
      'https://thumbs.dreamstime.com/z/barbells-gym-equipment-13487757.jpg',
    images: [
      {
        image:
          'https://cdn.shopify.com/s/files/1/2094/9815/products/10_Barbell_Rack_FBSR-ETE_1-WEB.jpg?v=1571708431',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQbiO3Dr7Ghxuk5aCOuyfbvA0Vx3qwqHxtpTA&usqp=CAU',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSh59XVOsGSfaxu9aIWJnK-jsA1wwCaPaulug&usqp=CAU',
      },
      {
        image:
          'https://www.vinexshop.com/Product-Images/Large/2581-OLYMPIC%20BARBELLS%20SET%20-%20COMPETITION.jpg',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUnUDjPsxQW4dOdgYLO5dFJa0scMLl20LrRA&usqp=CAU',
      },
    ],
    description: ` A complementary piece to the squat rack. A barbell is essential to strength training, it holds the free weights, or sometimes the weights are attached to the ends.

    A barbell is perhaps the most versatile out of all the gym equipment out there. You can literally do hundreds of different exercises with these iron bars.

    A common technique to use when utilizing a barbell is good posture and keeping your body from swaying; in order to make sure that your body stays stabilized, as when doing exercises on a gym machine for example.

    Because it is easy to cheat and swing your body whichever way to make the exercise easier, using a barbell comes with a word of caution: injuries.

    It is critical to keep good form when doing barbell exercises, particularly when picking them up off the ground and placing them back down, as during that period the risk of injury is particularly high!
    `,
    video: 'gPIAZlNJQsA',
  },
  {
    title: 'Exercise Ball',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcREcs88X_JLf_xJ-SyJ4HV9mb_JQLYA7Lhraw&usqp=CAU',
    images: [
      {
        image:
          'https://assetscdn1.paytm.com/images/catalog/product/S/SP/SPOPANDEY-SPORTSOLU2907859E31DD5/1562052109727_0..jpeg?imwidth=320&impolicy=hq',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSbmcZK8VpokGA6kzeRjb8SHNhyWbhJ0ZHSgA&usqp=CAU',
      },
      {
        image:
          'https://5.imimg.com/data5/VB/NS/TP/SELLER-29390614/61id-sajijl-sl1500--500x500.jpg',
      },
      {
        image:
          'https://www.theraband.com/media/catalog/product/cache/18/image/9df78eab33525d08d6e5fb8d27136e95/2/0/2008-c-theraband-exercise-and-stability-ball-standard-0.jpg',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS5g8adcci_I00NJCCvcDoBHhODOffUxsYoGA&usqp=CAU',
      },
    ],
    description: `An exercise ball, also known as a yoga ball, is a ball constructed of soft elastic with a diameter of approximately 35 to 85 centimeters (14 to 34 inches) and filled with air. The air pressure is changed by removing a valve stem and either filling with air or letting the ball deflate. It is most often used in physical therapy, athletic training and exercise. It can also be used for weight training.

    The ball, while often referred to as a Swiss ball, is also known by a number of different names, including balance ball, birth ball, body ball, ball, fitness ball, gym ball, gymnastic ball, physio ball, pilates ball, naval mine, Pezzi ball, stability ball, Swedish ball, or therapy ball.
    `,
    video: 'Kg6Ra5kbzW8',
  },
  {
    title: 'Dumbbells',
    image:
      'https://www.gymventures.com/content/images/wordpress/2016/01/DUMBBELLS.png',
    images: [
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTVQAiG6FBaGtmqovePRFxbgjYClROonh0DFQ&usqp=CAU&ec=45761791',
      },
      {
        image:
          'https://originfitness.com/wordpress/wp-content/uploads/2014/03/adjustable_dumbbells_nuo4-440x440.jpg',
      },
      {
        image:
          'https://images-na.ssl-images-amazon.com/images/I/31fNdT40ttL._SX425_.jpg',
      },
      {
        image:
          'https://spy.com/wp-content/uploads/2020/08/NordicTrack-feature-image.jpg?w=713',
      },
      {
        image:
          'https://images-eu.ssl-images-amazon.com/images/I/51Rvu6F-2tL.jpg',
      },
    ],
    description: ` Pretty much the go-to gym equipment most people first think of when they think of bodybuilding. Varying in weight, but the same concept, a handlebar with weights on opposite ends. A must-have free weight for any fitness regiment. There are even adjustable dumbbells with differing weights all in one piece.

    Dumbbells have a lot going for them, mainly, they are inexpensive when compared to other equipment. But if used right, they can offer just as much, if not more, than some of their more lucrative gym mates.

    When working out with dumbbells, try not to lock your elbows at the end of the movement, and do not force the last few repetitions if you do not have a friend or gym buddy to spot you.

    Dumbbells cause you to work other muscles as you are targeting a primary few; due to your body keeping balance and all the stabilizing muscles coming into play.
    `,
    video: 'l0gDqsSUtWo',
  },
  {
    title: 'BENCH PRESS',
    image:
      'https://www.gymventures.com/content/images/wordpress/2016/01/BENCH-PRESS.png',
    images: [
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSa3recM0xkW-k3bCll1otddIbimJ4cknMnfA&usqp=CAU',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQgBn637iP9WzXxlsqbdEANtRo8X1wT5Mlo8w&usqp=CAU',
      },
      {
        image:
          'https://cdn.shopify.com/s/files/1/1564/6971/articles/Smith_Machine_Bench_Press9e4732cc029a48877cd419a1b7cb0698_1024x1024.png?v=1566536718',
      },
      {
        image:
          'https://yorkbarbell.com/wp-content/uploads/2017/01/54038-55038-OlympicInclineBenchGunRacks.jpg',
      },
      {
        image:
          'https://www.bunnings.com.au/cdn-cgi/image/w=384,h=384/https://media-prod-use-1.mirakl.net/SOURCE/8f0ba18826ba49e79c193a264983cfaf',
      },
    ],
    description: ` Used for upper body strength training exercises, where you are pushing weight upwards as you’re laying on your back. Do you want the perfect chest? This is one of the tools you use to train for that chest.

    When doing a bench press, do not arch your back, this means that your feet are placed too low. Try not to bounce the bar off your chest either, even though you may think it makes you look super macho.

    Also, do not lower the bar towards your neck or abdomen as it could seriously injure you is you have excessive weight and lose control, even momentarily.

    Finally, try not to lock your elbows at the apex of the movement, keep your feet, back, and head still!
    `,
    video: 'UaOwz6DNdjw',
  },
  {
    title: 'HAMMER STRENGTH',
    image:
      'https://5.imimg.com/data5/UM/JH/MY-37112304/hammer-strength-machine-500x500.jpg',
    images: [
      {
        image:
          'https://tiimg.tistatic.com/fp/1/005/638/hammer-strength-plate-loaded-leg-press-606.jpg',
      },
      {
        image:
          'https://5.imimg.com/data5/XK/CB/MY-47479807/hammer-strength-plate-loaded-seated-500x500.jpg',
      },
      {
        image:
          'https://www.laijiangymequipment.com/photo/pl22849533-commercial_hammer_strength_plate_loaded_equipment_gym_fitness_seated_dip_machine.jpg',
      },
      {
        image:
          'https://www.lifefitness.com.au/wp-content/uploads/2020/07/CS-HSPL_PLSHC-hero.png',
      },
      {
        image:
          'https://static1.squarespace.com/static/54f212e1e4b0a87b1ad823e1/5b95d63d2b6a28d4f73d4058/5dc478c3ea53561fa1a4dc0e/1582147453248/hammer+adj+bench.jpg?format=1500w',
      },
    ],
    description: `A favorite among athletes, this mechanic focuses on explosiveness.

    Do not put too much demand on your triceps when exercising with the hammer strength machine. Also avoid locking your elbows at the top of the lift, and try your best to maintain an equal force in both arms as you’re lifting.
    
    Different types target different muscle groups. Primarily chest, then shoulders and triceps; to traps, and others.
    `,
    video: 'KpHDv8bdRU0',
  },
  {
    title: 'PULLUP BAR',
    image:
      'https://www.gymventures.com/content/images/wordpress/2016/01/PULLUP-BAR.png',
    images: [
      {
        image:
          'https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1591284480-41PKJRF8UKL.jpg?crop=1xw:1xh;center,top&resize=480%3A%2A',
      },
      {
        image:
          'https://i0.wp.com/www.anupamghose.com/wp-content/uploads/2019/08/41tkZcf8zCL.jpg?fit=500%2C455&ssl=1',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSPT7CLEptvG9ukRFRBuDAPxq43IwYt0jB2Mw&usqp=CAU',
      },
      {
        image:
          'https://i0.wp.com/post.healthline.com/wp-content/uploads/2020/12/479986-The-12-Best-Pullup-Bars-for-2020-Product-Image-Merax-Wall-Mounted-Pull-Up-Bar-product-image.png?w=840&h=840',
      },
      {
        image:
          'https://media.istockphoto.com/vectors/pullup-bar-in-3d-vector-illustration-vector-id683182340',
      },
    ],
    description: `The best tool for upper body exercise and strengthening. You can pull yourself up with any grip, though palms facing froward is the most popular one seen in demonstrations. Different grips and hand positions can affect different muscles. You pull yourself up until your chin is over the bar.

    Pullups are VERY demanding, especially for beginners. It is because of this that you must learn to master them. They are truly an excellent, and yes very demanding, upper body workout.

    Keep your movements vertical, aligned, and complete when doing pullups. After a while, if you become an expert at it, you can hang weight from your waist for additional resistance.

    If you are a beginner, you should probably start with a lat pull-down machine, which will build your muscles and get them ready for pullups.

    Mistakes include: doing the exercise rapidly, incomplete movement, swinging your body, bending of the torso forward to attain help from your chest muscles, bending knees to get momentum.

    Finally, do not extend your arms fully at the end of a downward movement.
    `,
    video: 'PYNPLenD_Yg',
  },
  {
    title: 'LEG PRESS MACHINE',
    image:
      'https://www.gymventures.com/content/images/wordpress/2016/01/LEG-PRESS-MACHINE.png',
    images: [
      {
        image:
          'https://images-na.ssl-images-amazon.com/images/I/61G1ZvY5e3L._SL1190_.jpg',
      },
      {
        image:
          'https://4.imimg.com/data4/BH/TW/MY-3142623/leg-press-machine-500x500.jpg',
      },
      {
        image:
          'https://s3.amazonaws.com/prod.skimble/assets/1209959/image_iphone.jpg',
      },
      {
        image: 'https://www.fitnessfactoryoutlet.com/images/products/14392.png',
      },
      {
        image:
          'https://i.pinimg.com/originals/6a/f5/35/6af5359e98b0c327dc02e55a5df7f2ef.jpg',
      },
    ],
    description: ` Ultimate leg training machine. Laying down with you back against a makeshift wall, push the platform/weights upwards; do not lock your joints. Perfect machine to train legs with.
    
    This machine is just as good as the squat machine as far as building strength and developing the muscle, however, it is good for protecting the back, due to the machine (with your help) holding it in position.

    Common mistakes fitness enthusiasts make when performing the leg press exercise are lifting the hips as the weight is lowered. Also, it is advised against ever locking your knees at the top of the movement, while your legs are fully extended.

    Incomplete and/or exaggerated movement will not result in an efficient workout either. Finally, avoid using too much or too little weight and be sure to push with both of your legs equally.
    `,
    video: 'IZxyjW7MPJQ',
  },
  {
    title: 'PUSHUP BAR',
    image:
      'https://garagegympro.com/wp-content/uploads/2018/09/Perfect-Fitness-Push-Up-Stands.jpg',
    images: [
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSIkZaWpGVUwLLPdJbIYtjW3-eNmUDxoPYJrQ&usqp=CAU',
      },
      {
        image:
          'https://images-na.ssl-images-amazon.com/images/I/61yGzRieLEL._SL1000_.jpg',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQBxBL6z_W3iDW4NdXj_UUiVC7gi9s_5rtquw&usqp=CAU',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSErkcE8ezUV3_L6DxqalP_DfcCjNrGJBzknw&usqp=CAU',
      },
      {
        image:
          'https://static.toiimg.com/thumb/msid-70346509,width-1200,height-900,resizemode-4/.jpg',
      },
    ],
    description: `Another cheap tool that can help you with your training. Alternatively going under the name press up bars or handles, these small and affordable push-up bars will improve your standard push-ups with more stability and by applying less pressure on your wrists.

    There are more studier and stable than dumbbells in regard to push ups. Doing push-ups with dumbbells is not recommended. Feel free to check them online or by checking our guide, maybe you will get yourself a pair right away.
    `,
    video: 'mw1LTdt2UzE',
  },
  {
    title: 'Seated row',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT0nEoJo-EzIlUg9QgmP3YkODnRr2Z-GssWvg&usqp=CAU',
    images: [
      {
        image:
          'https://images-na.ssl-images-amazon.com/images/I/51OqWxXltmL._SL1190_.jpg',
      },
      {
        image:
          'https://s3.amazonaws.com/prod.skimble/assets/840732/image_iphone.jpg',
      },
      {
        image:
          'https://www.verywellfit.com/thmb/mGmyxyyyHZjU5IddnygcIVeOUuM=/2324x1504/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/87395192-56aa07b65f9b58b7d0008802.jpg',
      },
      {
        image:
          'https://s3.amazonaws.com/prod.skimble/assets/1816205/image_iphone.jpg',
      },
      {
        image:
          'https://yorkbarbell.com/wp-content/uploads/2017/01/55018_55019_low-row-machine2_low.jpg',
      },
    ],
    description: ` The seated cable row also works on your lats, focussing on the mid-back to engage the back of the shoulders, biceps and rhomboids. If you sit at a desk all day, this exercise can help to strengthen the postural muscles, building a stronger back and better posture.
    `,
    video: 'xQNrFHEMhI4',
  },
  {
    title: 'KETTLEBELLS',
    image:
      'https://www.gymventures.com/content/images/wordpress/2016/01/KETTLEBELLS.png',
    images: [
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQIzuRdQ-WmsDUkIOXAp-yF7AzAX0uV8mbXjA&usqp=CAU',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSaGs-lqxkvurrgU1cmZ8gf5mwnCOYwwonF6g&usqp=CAU',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdSG6JdGrDEbNIWQGSDnjZJQ2I0N0ryInG3A&usqp=CAU',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ82-1XlDYKX2PjeHA852KepPMWlEVSdPFnGg&usqp=CAU',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSZCXxkzAvAQeo2FM7GQmRKZtm90JuoH_WdMQ&usqp=CAU',
      },
    ],
    description: `Kettlebells have become very popular in the fitness world, there are a ton of different exercises you can do with kettlebells. Fitness enthusiasts are using them for strength training and cardio. Take a look at the video for different ways to use kettlebells.
    `,
    video: 'RjnyMfZb00M',
  },
  {
    title: 'FOAM ROLLER',
    image:
      'https://www.gymventures.com/content/images/wordpress/2016/01/FOAM-ROLLER.png',
    images: [
      {
        image: 'https://www.tunturi.org/website/Accessoires/14TUSYO024.jpg',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQdDjtbMkAlNn6OzZd4O9bQ5ctLtV_axpVYTQ&usqp=CAU',
      },
      {
        image:
          'https://5.imimg.com/data5/UW/WN/WF/SELLER-16513912/yoga-foam-roller-blocks-use-for-massage-trigger-point-therapy-physio-exercise-train-gym--500x500.jpg',
      },
      {
        image:
          'https://cdn.shopify.com/s/files/1/1311/6023/products/roller_7_3_in_1_1000x1000.jpg?v=1527188996',
      },
      {
        image:
          'https://m.media-amazon.com/images/I/71J-2Dugh9L._AC._SR360,460.jpg',
      },
    ],
    description: `A perfect tool for reducing soreness, untying muscle knots, and increasing flexibility. The best way to use a foam roller is to incorporate it into your already existing fitness regiment, it will reduce stress on your body and help your body’s circulation.
    `,
    video: 'FhMCHJho6nU',
  },
  {
    title: 'Ab Roller',
    image:
      'https://garagegympro.com/wp-content/uploads/2018/10/Perfect-Fitness-Ab-Carver-Pro.jpg',
    images: [
      {
        image:
          'https://images-na.ssl-images-amazon.com/images/I/71MNCkCo82L._AC_SL1500_.jpg',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRqHav2iFItxvEtjN_O0UMJXvgwe7lWLAJE7g&usqp=CAU',
      },
      {
        image:
          'https://5.imimg.com/data5/YY/ZJ/YB/SELLER-13484669/abs-roller-4-wheel-ab-roller-abdominal-workout-fitness-exercise-equipment-500x500.jpeg',
      },
      {
        image:
          'https://www.insportline.eu/p132500/Ab-Roller-inSPORTline-AR1000.jpg',
      },
      {
        image:
          'https://www.gopherperformance.com/cmsstatic/img/602/F-06443-VariRollGluteRoller-model-3_Sharp-Sat.jpg?medium',
      },
    ],
    description: `Along with dip bars and push-up bars, ab roller is one of the most desired exercise tool for a home gym. Why? Simply because it is small, inexpensive and very useful.

    Its purpose lies in training abdominal muscles in a sitting position on your knees, where you roll the ab roller in front of you to a nearly-laying position. It is best however, to consult with a certified trainer how to do these exercises properly. Alternative, there is always YouTube.
    `,
    video: 'rqiTPdK1c_I',
  },
  {
    title: 'Resistance Bands',
    image:
      'https://garagegympro.com/wp-content/uploads/2019/01/WODFITTERS-Pull-Up-Resistance-Bands.jpg',
    images: [
      {
        image:
          'https://images-na.ssl-images-amazon.com/images/I/61NSI2w8suL._AC_SL1000_.jpg',
      },
      {
        image: 'https://i.ebayimg.com/images/g/iq4AAOSwhrRf2YIb/s-l640.jpg',
      },
      {
        image:
          'https://media.takealot.com/covers_images/0a85e124acec4336b95b7a44c42e7acb/s-pdpxl.file',
      },
      {
        image:
          'https://n1.sdlcdn.com/imgs/j/b/t/FITLETHIC-Hip-Resistance-Bands-Exercise-SDL889131238-1-209fc.jpeg',
      },
      {
        image:
          'https://target.scene7.com/is/image/Target/GUEST_6b3f25fd-0591-4ce7-ae6e-e23894728ae4?wid=488&hei=488&fmt=pjpeg',
      },
    ],
    description: `
    I found out that people tend to think that resistance bands and suspension trainers are the same thing. This is not the case. Resistance bands are basically stretchy pieces of plastic that use different weights of resistance. Resistance training increases one’s strength, for example by using them for bicep curls.

    There are a lot of great videos and exercises available online that you can check out. Also, the number of choices online to get the best resistance bands is crazy high, but fear not, here is your guide.
    `,
    video: 'FLNaG45ZiiA',
  },
  {
    title: 'Mini Exercise Bike',
    image:
      'https://garagegympro.com/wp-content/uploads/2019/01/DeskCycle-Under-Desk-Exercise-Bike.jpg',
    images: [
      {
        image:
          'https://images-na.ssl-images-amazon.com/images/I/813WiKYRSjL._SY450_.jpg',
      },
      {
        image:
          'https://images-na.ssl-images-amazon.com/images/I/61XpiCZPNgL._SY450_.jpg',
      },
      {
        image:
          'https://images-na.ssl-images-amazon.com/images/I/51q5UL4fbPL._AC_SL1000_.jpg',
      },
      {
        image:
          'https://5.imimg.com/data5/GT/MM/MY-3405603/presto-mini-exercise-bike-500x500.jpg',
      },
      {
        image:
          'https://i.pinimg.com/originals/40/92/b3/4092b34a8660f6d53619152b2f834a7a.png',
      },
    ],
    description: `
    These bikes are basically small variants of the standard exercise bikes. They are usually used by people who do not move much, who sit at a desk for 8 hours a day, or for people who have injured knees, since these machines use low-impact muscle movement.

    They are great tools if you have an office job and tend to spent a lot of hours at a desk, since most of them are so compact that they can be used under most tables and desks. They are usually also very cheap, so I’d definitely recommend using them. You can read more about these fellas in our guide.
    `,
    video: 'KVLv7vEZ3Rc',
  },
  {
    title: 'DIPPING BARS',
    image:
      'https://www.gymventures.com/content/images/wordpress/2016/01/DIPPING-BARS.png',
    images: [
      {
        image:
          'https://contents.mediadecathlon.com/p1103588/2000x2000/sq/100_dip_bar_training_station_domyos_by_decathlon_8379822_1103588.jpg?k=5cc7c279f32cbbd44a5abc4ab065cd54',
      },
      {
        image:
          'https://www.elitefts.com/media/catalog/product/cache/1/image/970x/602f0fa2c1f0d1ba5e241f914e856ff9/e/-/e-series-v-dip.jpg',
      },
      {
        image:
          'https://www.ironcompany.com/media/mf_webp/jpg/media/catalog/product/cache/c44567ae896e9ab304d8f9642c3da840/3/1/3146-xlg.webp',
      },
      {
        image:
          'https://images-na.ssl-images-amazon.com/images/I/41f8MBh%2B5tL.jpg',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRUqSa0TpjYSm0XfHHAkQ7aJzrD344N9UgJ9g&usqp=CAU',
      },
    ],
    description: `
    Holding onto the handles, one with each hand, lower your body and lift yourself up. It’s important to adhere to good form to avoid any potential shoulder injuries when dabbling with this gym equipment type.

    This is an excellent alternative to the decline press, but it does require a little more strength. So though typically not suggested for beginners, easing into it however is also not discouraged.

    When doing dips, you will notice that it feels as if your triceps are doing all the hard work and all the pressure is on them; it is essential however to focus on the pectoral muscle above all.

    You should feel the elongation as you dip and then the contraction as you push back up. Always perform this exercise slowly, unless you’re an athlete training for sports. Finally, avoid if you have suffered elbow or shoulder injuries.
    `,
    video: 'uA8y0FK9vEA1',
  },
  {
    title: 'ABDOMINAL BENCH',
    image:
      'https://www.gymventures.com/content/images/wordpress/2016/01/ABDOMINAL-BENCH.png',
    images: [
      {
        image:
          'https://images-na.ssl-images-amazon.com/images/I/71nWMJPMhLL._SX425_.jpg',
      },
      {
        image:
          'https://5.imimg.com/data5/YQ/TA/MY-2529750/sit-up-bench-500x500.jpg',
      },
      {
        image:
          'https://ae01.alicdn.com/kf/H4404bc2d4b724a539f0aee5836e7c8418.jpg',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSc7JAgT5G2Zqj4OQaeGzLLzTvDACkFvbsdsw&usqp=CAU',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShxMVDboUBqnlMGwctW85DaxDoCjmc9Ws6Ow&usqp=CAU',
      },
    ],
    description: `
    Designed with your abs in mind. It’s similar to the hyperextension machine, but for your abs. Important to practice and adhere to proper form when performing this exercise. Especially if you decide to hold onto weights as you’re lifting yourself up.

    Doing this exercise also causes your hips to flex, which brings in the muscles that work that area, the psoas, and the front of the quadriceps in particular.

    Thus, it is not as specific as just the crunch, so be sure to feel the muscles as you are working out, because more than one group should be involved, and you want to make sure your torso not as rigid, in order to focus more on the abdominal muscles. A few types of sit-ups to try are twisting, weighted, incline bench, and vertical bench.
    `,
    video: '018EClOtVTM',
  },
  {
    title: 'PECK DECK MACHINE',
    image:
      'https://www.gymventures.com/content/images/wordpress/2016/01/PECK-DECK-MACHINE.png',
    images: [
      {
        image:
          'https://5.imimg.com/data5/VT/EB/PZ/SELLER-8753206/pec-deck-machine-500x500.jpg',
      },
      {
        image:
          'https://5.imimg.com/data5/RC/PG/MY-57979463/pec-deck-fly-machine-500x500.jpg',
      },
      {
        image:
          'https://www.elitefts.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/i/-/i-pdrdsel-co.jpg',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRl8r8qdHG8avC8ON7wn5-oFx27rKs1sYV1_Q&usqp=CAU',
      },
      {
        image:
          'https://s3.amazonaws.com/images.myfit.ca/cache/images/machineflyes_002.gif',
      },
    ],
    description: `
    This type of gym equipment was designed to isolate and work the chest. A favorite for beginner bodybuilders as it has a unique motion that feels good when performing.

    To properly use the peck deck machine, sit back with your elbows at about 90 degrees, on the mid-chest level. Push to bring the arms together in front of your face,

    Breathe in as you are opening your arms, going as far back as flexibility will allow (but be careful if you’re using heavy weights); breathe out as you bring the arms together again.

    Common mistakes include too little travel and too much travel. Too little reduces the effect of the exercise and too much can cause injury. Do not separate the elbows from the bar, meaning you’re pushing with your hands instead.
    `,
    video: 'LMu_7Zi_AX0',
  },
  {
    title: 'Preacher Curl Bench',
    image:
      'https://garagegympro.com/wp-content/uploads/2019/07/GOPLUS-Preacher-Curl-Weight-Bench.jpg',
    images: [
      {
        image:
          'https://www.tuffstuffitness.com/wp-content/uploads/Proformance-Plus-Preacher-Curl-Bench-PPF-706-800x800.jpg',
      },
      {
        image:
          'https://www.rehabmart.com/include-mt/img-resize.asp?path=/imagesfromrd/bs-gpcb329.jpg&newwidth=650',
      },
      {
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcReSEZc5a5NZWkj0lHGh7XzQHrKx3SGW5d-yg&usqp=CAU',
      },
      {
        image: 'https://m.media-amazon.com/images/I/414a+hSVjtL._SL160_.jpg',
      },
      {
        image:
          'https://cpimg.tistatic.com/05401391/b/4/Preacher-Curl-Bench.jpg',
      },
    ],
    description: `
    This piece of equipment focuses on bicep development, specifically targets brachialis (lower bicep). Preacher curl benches can use either weight plates, or an included weight stack. But usually, weight stacks are included in commercial-grade equipment only.
    `,
    video: '1-xvtHS9PsU',
  },
];

const data = [
  {
    type: KEY_GRID,
    data: datas,
    columns: 3,
    // header: '',
  },
];

export default data;
